import React, {memo} from "react";
import LeftMenu from "../organisms/LeftMenu/LeftMenu";
import Div from "../atoms/View/Div";
import Container from "../organisms/Container/Container";

function Dashboard() {
    return (
        <Div className="wrapper">
            <LeftMenu/>
            <Container/>
        </Div>
    )
}

export default memo(Dashboard)