import React, {memo} from 'react';
import Div from "../../atoms/View/Div";
import Heading from "../Heading/Heading";
import ContainerContentBoxOne from "../ContainerContentBoxOne/ContainerContentBoxOne";
import ContainerContentBoxTwo from "../ContainerContentBoxTwo/ContainerContentBoxTwo";


function ContainerContent() {
    return (
        <Div className="content-page">
            <Div className="content">
                <Heading/>
                <Div className="page-content-wrapper">
                    <Div className="container-fluid">
                        <ContainerContentBoxOne/>
                        <ContainerContentBoxTwo/>
                    </Div>
                </Div>
            </Div>
        </Div>
    )
}

export default memo(ContainerContent)