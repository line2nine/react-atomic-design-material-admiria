import React, {memo} from 'react';
import Div from "../../atoms/View/Div";
import Span from "../../atoms/View/Span";
import A from "../../atoms/A/A";
import Nav from "../../atoms/Nav/Nav";
import UL from "../../atoms/View/UL";
import Li from "../../atoms/View/Li";
import Image from "../../atoms/Image/Image";
import H from "../../atoms/H/H";
import HR from "../../atoms/HR/HR";
import Label from "../../atoms/Label/Label";
import Input from "../../atoms/Input/Input";


function ContainerContentBoxOne() {
    return (
        <Div className="row">
            <Div className="col-md-6 col-xl-6">
                <Div className="mini-stat clearfix bg-white">
                    <H className="h4-c">계정 정보</H>
                    <Div className="row">
                        <Div className="col-md-6">
                            <Label className="label-form" htmlFor="username">ID</Label>
                            <Input type="text" name="username" id="username" className="form-control"
                                   placeholder="경호"/>
                        </Div>
                        <Div className="col-md-6">
                            <Label className="label-form" htmlFor="password">PASSWORD</Label>
                            <Input type="text" name="password" id="password" className="form-control"
                                   placeholder="강"/>
                        </Div>
                    </Div>
                </Div>
            </Div>
            <Div className="col-md-6 col-xl-6">
                <Div className="mini-stat clearfix bg-white">
                    <H className="h4-c">개인 정보</H>
                    <Div className="row">
                        <Div className="col-md-6">
                            <Label className="label-form" htmlFor="username-2">이름</Label>
                            <Input type="text" name="username" id="username-2" className="form-control"
                                   placeholder="경호"/>
                        </Div>
                        <Div className="col-md-6">
                            <Label className="label-form" htmlFor="password-2">성</Label>
                            <Input type="text" name="password" id="password-2" className="form-control"
                                   placeholder="강"/>
                        </Div>
                        <Div className="col-md-12">
                            <Label className="label-form" htmlFor="hidden-2">이메일 주소</Label>
                            <Input type="text" name="password" id="hidden-2" className="form-control"
                                   placeholder="k-web@naver.cm"/>
                        </Div>
                    </Div>
                </Div>
            </Div>
        </Div>
    )
}

export default memo(ContainerContentBoxOne)