import React, {memo} from 'react';
import Div from "../../atoms/View/Div";
import Span from "../../atoms/View/Span";
import A from "../../atoms/A/A";
import Nav from "../../atoms/Nav/Nav";
import UL from "../../atoms/View/UL";
import Li from "../../atoms/View/Li";
import Image from "../../atoms/Image/Image";
import H from "../../atoms/H/H";
import HR from "../../atoms/HR/HR";
import Label from "../../atoms/Label/Label";
import Input from "../../atoms/Input/Input";
import Button from "../../atoms/Button/Button";
import Table from "../../atoms/Table/Table";
import TR from "../../atoms/Table/TR";
import TD from "../../atoms/Table/TD";


function ContainerContentBoxTwo() {
    function handleOnClick() {
        return alert("Test click");
    }
    return (
        <Div className="row">
            <Div className="col-xl-12">
                <Div className="row">
                    <Div className="col-md-12 pr-md-0">
                        <Div className="card m-b-20" style={{height: "auto"}}>
                            <Div className="card-body">
                                <Div className="card-input-checkbox">
                                    <H className="h4-c">권한</H>
                                    <br/>
                                        <Div>
                                            <Input className="sz-16" type="checkbox" name="ckb-1" id="ckb-1" checked={true}/>
                                            <Span> 활성</Span>
                                            <Label className="label-form" htmlFor="ckb-1">&nbsp;(이 사용자가 활성화되어 있는지를 나타냅니다. 계정을 삭제하는 대신 이것을 선택
                                                해제하세요.)</Label>
                                        </Div>
                                        <Div>
                                            <Input className="sz-16" type="checkbox" name="ckb-2" id="ckb-2" checked={true}/>
                                            <Span> 스태프 권한</Span>
                                            <Label className="label-form" htmlFor="ckb-2">&nbsp;(사용자가 관리사이트에 로그인이 가능한지를 나타냅니다.)</Label>
                                        </Div>
                                        <Div>
                                            <Input className="sz-16" type="checkbox" name="ckb-3" id="ckb-3" checked={true}/>
                                            <Span> 최상위 사용자 권한</Span>
                                            <Label className="label-form" htmlFor="ckb-3">&nbsp;(해당 사용자에게 모든 권한을 허가 합니다.)</Label>
                                        </Div>
                                </Div>
                                <HR className="display"/>
                                    <Div className="card-input-table">
                                        <Div className="row">
                                            <Div className="col-md-6">
                                                <h6>그룹</h6>
                                                <Div className="row">
                                                    <Div className="col-md-5">
                                                        <Div className="mini-table">
                                                            <Div>
                                                                <Div>
                                                                    <Span>이용 가능한 그룹</Span>
                                                                    <Button
                                                                        className="btn btn-success btn-sm btn-float-right">
                                                                        전체선택
                                                                    </Button>
                                                                </Div>
                                                                <Div>
                                                                    <Label htmlFor="search-1">

                                                                    </Label>
                                                                    <Div className="row">
                                                                        <Input type="search" name="search-1"
                                                                               id="search-1"
                                                                               className="form-control"
                                                                               style={{width: "242px"}}/>
                                                                        <Button className="btn btn-dark btn-sm search-box">
                                                                            <i className="fa fa-search"/>
                                                                        </Button>
                                                                    </Div>
                                                                </Div>
                                                                <br/>
                                                                    <Div>
                                                                        <Table className="table-content">
                                                                            <TR>
                                                                                <TD>&nbsp;</TD>
                                                                            </TR>
                                                                            <TR>
                                                                                <TD>&nbsp;</TD>
                                                                            </TR>
                                                                            <TR>
                                                                                <TD>&nbsp;</TD>
                                                                            </TR>
                                                                            <TR>
                                                                                <TD>&nbsp;</TD>
                                                                            </TR>
                                                                            <TR>
                                                                                <TD>&nbsp;</TD>
                                                                            </TR>
                                                                        </Table>
                                                                    </Div>
                                                            </Div>
                                                        </Div>
                                                    </Div>
                                                    <Div>
                                                        <Div className="mgt-105">
                                                            <Div className="mb-2">
                                                                <Button className="btn btn-outline-dark btn-sm">
                                                                    <i className="mdi mdi-chevron-right"/>
                                                                </Button>
                                                            </Div>
                                                            <Div>
                                                                <Button className="btn btn-outline-dark btn-sm">
                                                                    <i className="mdi mdi-chevron-left"/>
                                                                </Button>
                                                            </Div>
                                                        </Div>
                                                    </Div>
                                                    <Div className="col-md-5">
                                                        <Div className="mini-table">
                                                            <Div>
                                                                <Span>선택된 그룹</Span>
                                                                <Button
                                                                    className="btn btn-success btn-sm btn-float-right">
                                                                    전체선택
                                                                </Button>
                                                            </Div>
                                                            <Div>
                                                                <Label htmlFor="search-2">

                                                                </Label>
                                                                <Div className="row">
                                                                    <Input type="search" name="search-2"
                                                                           id="search-2"
                                                                           className="form-control"
                                                                           style={{width: "242px"}}/>
                                                                    <Button className="btn btn-dark btn-sm search-box">
                                                                        <i className="fa fa-search"/>
                                                                    </Button>
                                                                </Div>
                                                            </Div>
                                                            <br/>
                                                                <Div>
                                                                    <Table className="table-content">
                                                                        <TR>
                                                                            <TD>default_1</TD>
                                                                        </TR>
                                                                        <TR>
                                                                            <TD>selected_1</TD>
                                                                        </TR>
                                                                        <TR>
                                                                            <TD>selected_2</TD>
                                                                        </TR>
                                                                        <TR>
                                                                            <TD>&nbsp;</TD>
                                                                        </TR>
                                                                        <TR>
                                                                            <TD>&nbsp;</TD>
                                                                        </TR>
                                                                    </Table>
                                                                </Div>
                                                        </Div>
                                                    </Div>
                                                </Div>
                                                <Div>
                                                    <p>
                                                        이 사용자가 속한 그룹, 사용자는 그룹에 부여된 모든 권한을 몰려 받습니다.
                                                    </p>
                                                    <p>
                                                        하나 이상을 선택하려면 "Control"키, Mac은 "Command"키를 누르세요.
                                                    </p>
                                                </Div>
                                            </Div>
                                            <Div className="col-md-6">
                                                <h6>사용자 권한</h6>
                                                <Div className="row">
                                                    <Div className="col-md-5">
                                                        <Div className="mini-table">
                                                            <Div>
                                                                <Div>
                                                                    <Span>이용 가능한 사용자 권한</Span>
                                                                    <Button
                                                                        className="btn btn-success btn-sm btn-float-right">
                                                                        전체선택
                                                                    </Button>
                                                                </Div>
                                                                <Div>
                                                                    <Label htmlFor="search-3">

                                                                    </Label>
                                                                    <Div className="row">
                                                                        <Input type="search" name="search-3"
                                                                               id="search-3"
                                                                               className="form-control"
                                                                               style={{width: "242px"}}/>
                                                                        <Button className="btn btn-dark btn-sm search-box">
                                                                            <i className="fa fa-search"/>
                                                                        </Button>
                                                                    </Div>
                                                                </Div>
                                                                <br/>
                                                                    <Div>
                                                                        <Table className="table-content">
                                                                            <TR>
                                                                                <TD>&nbsp;</TD>
                                                                            </TR>
                                                                            <TR>
                                                                                <TD>&nbsp;</TD>
                                                                            </TR>
                                                                            <TR>
                                                                                <TD>&nbsp;</TD>
                                                                            </TR>
                                                                            <TR>
                                                                                <TD>&nbsp;</TD>
                                                                            </TR>
                                                                            <TR>
                                                                                <TD>&nbsp;</TD>
                                                                            </TR>
                                                                        </Table>
                                                                    </Div>
                                                            </Div>
                                                        </Div>
                                                    </Div>
                                                    <Div>
                                                        <Div className="mgt-105">
                                                            <Div className="mb-2">
                                                                <Button className="btn btn-outline-dark btn-sm">
                                                                    <i className="mdi mdi-chevron-right"/>
                                                                </Button>
                                                            </Div>
                                                            <Div>
                                                                <Button className="btn btn-outline-dark btn-sm">
                                                                    <i className="mdi mdi-chevron-left"/>
                                                                </Button>
                                                            </Div>
                                                        </Div>
                                                    </Div>
                                                    <Div className="col-md-5">
                                                        <Div className="mini-table">
                                                            <Div>
                                                                <Span>선택된 사용자 권</Span>
                                                                <Button
                                                                    className="btn btn-success btn-sm btn-float-right">
                                                                    전체선택
                                                                </Button>
                                                            </Div>
                                                            <Div>
                                                                <Label htmlFor="search-4">

                                                                </Label>
                                                                <Div className="row">
                                                                    <Input type="search" name="search-4"
                                                                           id="search-4"
                                                                           className="form-control"
                                                                           style={{width: "242px"}}/>
                                                                    <Button className="btn btn-dark btn-sm search-box">
                                                                        <i className="fa fa-search"/>
                                                                    </Button>
                                                                </Div>
                                                            </Div>
                                                            <br/>
                                                                <Div>
                                                                    <Table className="table-content">
                                                                        <TR>
                                                                            <TD>default_1</TD>
                                                                        </TR>
                                                                        <TR>
                                                                            <TD>selected_1</TD>
                                                                        </TR>
                                                                        <TR>
                                                                            <TD>selected_2</TD>
                                                                        </TR>
                                                                        <TR>
                                                                            <TD>&nbsp;</TD>
                                                                        </TR>
                                                                        <TR>
                                                                            <TD>&nbsp;</TD>
                                                                        </TR>
                                                                    </Table>
                                                                </Div>
                                                        </Div>
                                                    </Div>
                                                </Div>
                                                <Div>
                                                    <p>
                                                        이 사용자가 특정 권한, 하나 이상을 선택하려면 "Control"키, Mac은 "Command"키를
                                                        누르세요.
                                                    </p>
                                                </Div>
                                            </Div>
                                        </Div>
                                    </Div>
                            </Div>
                        </Div>
                        <br/>
                        <Div className="row">
                            <Div className="col-md-6">
                                <Div>
                                    <Button className="btn btn-danger btn-sm" onClick={handleOnClick}>
                                        <i className="fa fa-trash"/>
                                        삭제
                                    </Button>
                                </Div>
                            </Div>
                            <Div className="col-md-6">
                                <Div className="text-right">
                                    <Button className="btn btn-primary btn-sm" onClick={handleOnClick}>
                                        저장 및 다름 이름으로 추가 &nbsp;
                                        <i className="fa fa-play"/>
                                    </Button>
                                    <Button className="btn btn-info btn-sm" onClick={handleOnClick}>
                                        저장 및 편집 계속 &nbsp;
                                        <i className="fa fa-play"/>
                                    </Button>
                                    <Button className="btn btn-success btn-sm" onClick={handleOnClick}>
                                        <i className="fa fa-save"/>
                                        저장
                                    </Button>
                                </Div>
                            </Div>
                        </Div>
                    </Div>
                </Div>
            </Div>
        </Div>
    )
}

export default memo(ContainerContentBoxTwo)