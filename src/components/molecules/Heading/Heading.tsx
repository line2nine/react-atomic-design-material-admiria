import React, {useState, useEffect, memo} from 'react';
import Div from "../../atoms/View/Div";
import Span from "../../atoms/View/Span";
import A from "../../atoms/A/A";
import Nav from "../../atoms/Nav/Nav";
import UL from "../../atoms/View/UL";
import Li from "../../atoms/View/Li";
import Image from "../../atoms/Image/Image";
import H from "../../atoms/H/H";
import HR from "../../atoms/HR/HR";


function Heading() {
    return (
        <Div className="topbar">
            <Nav className="navbar-custom">
                <UL className="list-inline float-right mb-0">
                    <Li className="list-inline-item dropdown notification-list">
                        <A className="nav-link dropdown-toggle arrow-none waves-effect nav-user">
                            <Image src="resources/assets/images/icon/user_1.png" alt="user"
                                 className="rounded-circle user-avatar"/>
                        </A>
                    </Li>
                </UL>

                <UL className="list-inline menu-left mb-0 header-color">
                    <Li className="hide-phone list-inline-item app-search">
                        <H className="page-title">Account</H>
                    </Li>
                </UL>
                <HR/>
            </Nav>
        </Div>
    )
}

export default memo(Heading)