import React, { memo } from 'react';
import Div from "../../atoms/View/Div";
import A from "../../atoms/A/A";
import Span from "../../atoms/View/Span";
import UL from "../../atoms/View/UL";
import Li from "../../atoms/View/Li";

function LeftMenuList() {
    return (
        <Div className="sidebar-inner">
            <Div className="sidebar-menu">
                <UL>
                    <Li className="has_sub nav-active">
                        <A href="#"><Span> Account <Span className="pull-right">
                            <i className="mdi mdi-chevron-right"></i></Span> </Span></A>
                    </Li>
                    <Li className="has_sub">
                        <A href="#"><Span> Groups <Span className="pull-right">
                            <i className="mdi mdi-chevron-right"></i></Span> </Span></A>
                    </Li>
                    <Li className="has_sub">
                        <A href="#"><Span> 시스템 로그 <Span className="pull-right">
                            <i className="mdi mdi-chevron-right"></i></Span> </Span></A>
                    </Li>
                    <Li className="has_sub">
                        <A href="#"><Span> 시스템 통계 <Span className="pull-right">
                            <i className="mdi mdi-chevron-right"></i></Span> </Span></A>
                    </Li>
                </UL>
            </Div>
        </Div>
    )
}

export default memo(LeftMenuList)