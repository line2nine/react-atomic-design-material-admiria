import React, {memo} from 'react';
import styled from 'styled-components';
import cn from 'classnames';

interface LabelProps {
    children?: React.ReactNode;
    className?: string;
    htmlFor: any
}

const StyledLabel = styled.label<LabelProps>`
  &.label-form {font-size: 12px !important; color: #999999;}
`;

const Label = ({
                   children,
                   className,
                   htmlFor
               }: LabelProps) => {
    const classCandidate = [className];
    const commonProps = {};
    return <StyledLabel {...commonProps} className={cn(classCandidate)} htmlFor={htmlFor}>
        {children}
    </StyledLabel>
};

export default memo(Label);