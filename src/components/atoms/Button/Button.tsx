import React, {memo} from 'react';
import styled from 'styled-components';
import cn from 'classnames';

interface ButtonProps {
    children?: React.ReactNode;
    className?: string;

    [prop: string]: any;

    style?: any;

    onClick?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
}

const StyledButton = styled.button<ButtonProps>`
  &.btn-float-right {float: right; margin-top: -8px}
  &.btn {border-radius: 3px;}

  &.btn-primary, &.btn-success, &.btn-info, &.btn-warning,
  &.btn-danger, &&.btn-dark, &.btn-pink, &.btn-purple, &.btn-indigo, &.btn-teal,
  &.btn-lime, &.btn-orange, &.btn-brown, &.btn-blue-grey {
    color: #ffffff;
  }

  &.btn-primary {background-color: #4a81c5; border: 1px solid #4a81c5;}

  &.btn-primary:hover, &.btn-primary:focus, &.btn-primary:active, &.btn-primary.active,
  &.btn-primary.focus, &.btn-primary:active, &.btn-primary:focus, &.btn-primary:hover,
  .open > .dropdown-toggle&.btn-primary, &.btn-outline-primary.active, &.btn-outline-primary:active,
  .show > &.btn-outline-primary.dropdown-toggle, &.btn-outline-primary:hover, &.btn-primary.active,
  &.btn-primary:active, .show > &.btn-primary.dropdown-toggle {
    background-color: #3c6597 !important;
    border: 1px solid #3c6597 !important;
  }

  &.btn-primary.focus, &.btn-primary:focus, &.btn-outline-primary.focus, &.btn-outline-primary:focus {
    -webkit-box-shadow: 0 0 0 2px rgba(103, 168, 228, 0.3);
    box-shadow: 0 0 0 2px rgba(103, 168, 228, 0.3);
  }

  &.btn-success {background-color: #289d19 !important; border: 1px solid #289d19 !important;}

  &.btn-success:hover, &.btn-success:focus, &.btn-success:active, &.btn-success.active,
  &.btn-success.focus, &.btn-success:active, &.btn-success:focus, &.btn-success:hover,
  .open > .dropdown-toggle&.btn-success, &.btn-outline-success.active, &.btn-outline-success:active,
  .show > &.btn-outline-success.dropdown-toggle, &.btn-outline-success:hover, &.btn-success.active,
  &.btn-success:active, .show > &.btn-success.dropdown-toggle {
    background-color: #1e7413 !important;
    border: 1px solid #1e7413 !important;
  }

  &.btn-info {background-color: #2b5487; border: 1px solid #2b5487;}

  &.btn-info:hover, &.btn-info:focus, &.btn-info:active, &.btn-info.active, &.btn-info.focus,
  &.btn-info:active, &.btn-info:focus, &.btn-info:hover, .open > .dropdown-toggle&.btn-info,
  &.btn-outline-info.active, &.btn-outline-info:active,
  .show > &.btn-outline-info.dropdown-toggle, &.btn-outline-info:hover, &.btn-info.active, &.btn-info:active,
  .show > &.btn-info.dropdown-toggle {
    background-color: #22436b !important;
    border: 1px solid #22436b !important;
  }

  &.btn-danger {background-color: #d21f1b !important; border: 1px solid #d21f1b !important;}

  &.btn-danger:active, &.btn-danger:focus, &.btn-danger:hover, &.btn-danger.active,
  &.btn-danger.focus, &.btn-danger:active, &.btn-danger:focus, &.btn-danger:hover,
  .open > .dropdown-toggle&.btn-danger, &.btn-outline-danger.active, &.btn-outline-danger:active,
  .show > &.btn-outline-danger.dropdown-toggle, &.btn-outline-danger:hover, &.btn-danger.active,
  &.btn-danger:active, .show > &.btn-danger.dropdown-toggle {
    background-color: #aa1d1a !important;
    border: 1px solid #aa1d1a !important;
  }

  &.btn-danger.focus, &.btn-danger:focus, &.btn-outline-danger.focus, &.btn-outline-danger:focus {
    -webkit-box-shadow: 0 0 0 2px rgba(234, 85, 61, 0.3);
    box-shadow: 0 0 0 2px rgba(234, 85, 61, 0.3);
  }
  &.btn-dark {
    background-color: #111111 !important;
    border: 1px solid #111111 !important;
    color: #ffffff !important;
  }

  &.btn-dark:hover, &.btn-dark:focus, &.btn-dark:active, &.btn-dark.active, &.btn-dark.focus,
  &.btn-dark:active, &.btn-dark:focus, &.btn-dark:hover, .open > .dropdown-toggle&.btn-dark,
  &.btn-outline-dark.active, &.btn-outline-dark:active,
  &.btn-outline-dark.dropdown-toggle {
    background-color: #fff !important;
    border: 1px solid #111111 !important;
    color: #111111 !important;
  }
  &.btn-dark.focus, &.btn-dark:focus, &.btn-outline-dark.focus, &.btn-outline-dark:focus {
    -webkit-box-shadow: 0 0 0 2px rgba(29, 30, 58, 0.3);
    box-shadow: 0 0 0 2px rgba(29, 30, 58, 0.3);
  }
  &.btn-outline-dark:hover {
    color: #fff !important;
    background-color: #111111 !important;
    border-color: #111111 !important;
  }
  
`;

const Button = ({
                    children,
                    className,
                    style,
    onClick
                }: ButtonProps) => {
    const classCandidate = [className];
    const commonProps = {
    };
    return <StyledButton {...commonProps} className={cn(classCandidate)} style={style} onClick={onClick}>
        {children}
    </StyledButton>
};

export default memo(Button);
