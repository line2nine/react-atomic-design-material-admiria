import React, {memo} from 'react';
import styled from 'styled-components';
import cn from 'classnames';

interface TableProps {
    children?: React.ReactNode;
    flex?: number | 'auto';
    className?: string;

    [prop: string]: any;

    onClick?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
}

const StyledTable = styled.table<TableProps>`
  &.table-content {background-color: #fff; border: 1px solid rgba(112, 112, 112, 0.12);}
  &.table-content tr:hover {
    cursor: pointer;
    background-color: #e1e1e1;
    }
`;

const Table = ({
                     children,
                     flex = 'auto',
                     className,
                 }: TableProps) => {
    const classCandidate = [className];
    const commonProps = {
        flex,
    };
    return <StyledTable {...commonProps} className={cn(classCandidate)}>
        {children}
    </StyledTable>
};

export default memo(Table);
