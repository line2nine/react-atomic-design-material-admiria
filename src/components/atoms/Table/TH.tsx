import React, {memo} from 'react';
import styled from 'styled-components';
import cn from 'classnames';

interface THProps {
    children?: React.ReactNode;
    flex?: number | 'auto';
    className?: string;

    [prop: string]: any;

    onClick?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
}

const StyledTH = styled.th<THProps>`
    &.left-text {
      text-align: left;
    }
`;

const TH = ({
                children,
                flex = 'auto',
                className,
            }: THProps) => {
    const classCandidate = [className];
    const commonProps = {
        flex,
    };
    return <StyledTH {...commonProps} className={cn(classCandidate)}>
        {children}
    </StyledTH>
};

export default memo(TH);
