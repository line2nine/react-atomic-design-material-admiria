import React, {memo} from 'react';
import styled from 'styled-components';
import cn from 'classnames';

interface AProps {
    children?: React.ReactNode;
    className?: string;
    url?: string;

    [prop: string]: any;

    onClick?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
}

const StyledA = styled.a<AProps>`
  cursor: pointer;
  &.logo {line-height: 80px;}
  &.nav-link {padding: 0 12px; line-height: 70px;}
  &.arrow-none:after {border: none; margin: 0; display: none;}
  &.waves-effect {
    position: relative;
    cursor: pointer;
    display: inline-block;
    overflow: hidden;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    -webkit-tap-highlight-color: transparent;
    vertical-align: middle;
    z-index: 1;
    will-change: opacity, transform;
    -webkit-transition: all 0.3s ease-out;
    transition: all 0.3s ease-out;
  }
  &.nav-user {margin-right: 10px;}
`;

const A = ({
               children,
               className,
           }: AProps) => {
    const classCandidate = [className];
    const commonProps = {};
    return <StyledA {...commonProps} className={cn(classCandidate)}>
        {children}
    </StyledA>
};

export default memo(A);
