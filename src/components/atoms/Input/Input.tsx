import React, {memo} from 'react';
import styled from 'styled-components';
import cn from 'classnames';

interface InputProps {
    className?: string;
    type?: string;
    name?: string;
    id?: string;
    value?: any;
    placeholder?: string;
    style?: any;
    checked?: boolean;
}

const StyledInput = styled.input<InputProps>`

`;

const Input = ({
                   className,
                   type,
                   name,
                   id,
                   value,
                   placeholder,
                   style,
                   checked
               }: InputProps) => {
    const classCandidate = [className];
    const commonProps = {};
    return <StyledInput {...commonProps} className={cn(classCandidate)} type={type} name={name} id={id} value={value} placeholder={placeholder} style={style} checked={checked}/>
};

export default memo(Input);