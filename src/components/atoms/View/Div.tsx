import React, {memo} from 'react';
import styled from 'styled-components';
import cn from 'classnames';

interface DivProps {
    children?: React.ReactNode;
    className?: string;

    [prop: string]: any;
    style?: any;

    onClick?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
}

const StyledDiv = styled.div<DivProps>`
  &.wrapper {height: 100%; overflow: hidden; width: 100%;}
  &.left {bottom: 50px; height: 100%; margin-bottom: -70px; margin-top: 0; padding-bottom: 70px; position: fixed;}
  &.side-menu {
    bottom: 0;
    top: 0;
    width: 240px;
    -webkit-transition: all 0.4s ease-in-out;
    transition: all 0.4s ease-in-out;
    background: #121b26;
    position: absolute;
    z-index: 99;
  }
  &.topbar-left {padding-left: 30px;}
  &.sidebar-inner {height: 100%;}
  &.sidebar-menu {background-color: #121b26; padding-bottom: 50px; width: 100%;}
  &.sidebar-menu, &.sidebar-menu ul, &.sidebar-menu li, &.sidebar-menu a {
    border: 0;
    font-weight: normal;
    line-height: 1;
    list-style: none;
    margin: 0;
    padding: 0;
    position: relative;
    text-decoration: none;
  }
  &.sidebar-menu > ul > li > a {color: rgba(255, 255, 255, 0.5); display: block; padding: 12px 20px; background-color: #121b26;}
  &.sidebar-menu > ul > li > a:hover {color: rgba(255, 255, 255, 0.8); text-decoration: none;}
  &.sidebar-menu > ul > li.nav-active > a {background-color: #0b131c; color: rgba(255, 255, 255, 0.8);}
  &.sidebar-menu > ul > li > a > span {vertical-align: middle;}
  &.sidebar-menu > ul > li > a span i {font-size: 18px; line-height: 22px;}
  &.sidebar-menu > ul > li > a > i {
    display: inline-block;
    font-size: 18px;
    line-height: 17px;
    margin-left: 3px;
    margin-right: 10px;
    text-align: center;
    vertical-align: middle;
    width: 20px;
  }
  &.sidebar-menu > ul > li > a > i.i-right {float: right; margin: 3px 0 0 0;}
  &.sidebar-menu > ul > li > a.active {color: rgba(255, 255, 255, 0.8);}
  &.sidebar-menu > ul > li > a.active i {color: rgba(255, 255, 255, 0.8);}

  &.content-page {margin-left: 240px; overflow: hidden; background-color: #efefef;}
  //&.content {margin-bottom: 60px; -webkit-transition: all 0.4s ease-in-out; transition: all 0.4s ease-in-out;}
  &.content {margin-bottom: 60px; padding: 20px; -webkit-transition: all 0.4s ease-in-out; transition: all 0.4s ease-in-out;}
  &.topbar {position: fixed; left: 240px; right: 0; top: 0; z-index: 999;}

  &.page-content-wrapper {margin: 0 -10px; padding-top: 80px;}
  &.mini-stat {border: 1px solid rgba(112, 112, 112, 0.12); padding: 20px; border-radius: 10px; margin-bottom: 20px; height: 204px;}
  
  &.card {border: 1px solid rgba(112, 112, 112, 0.12); border-radius: 10px;}
  &.mini-table {border: 1px solid rgba(112, 112, 112, 0.12); padding: 20px; border-radius: 10px; margin-bottom: 20px; background-color: #f7f7f7;}
`;

const Div = ({
                  children,
                  className,
                 style
              }: DivProps) => {
    const classCandidate = [className];
    const commonProps = {
    };
    return <StyledDiv {...commonProps} className={cn(classCandidate)} style={style}>
            {children}
        </StyledDiv>
};

export default memo(Div);
