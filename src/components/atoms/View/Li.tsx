import React, {memo} from 'react';
import styled from 'styled-components';
import cn from 'classnames';

interface LiProps {
    children?: React.ReactNode;
    className?: string;

    [prop: string]: any;

    onClick?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
}

const StyledLi = styled.li<LiProps>`
  &.notification-list {margin-left: 0;}
`;

const Li = ({
                  children,
                  className,
              }: LiProps) => {
    const classCandidate = [className];
    const commonProps = {
    };
    return <StyledLi {...commonProps} className={cn(classCandidate)}>
            {children}
        </StyledLi>
};

export default memo(Li);
