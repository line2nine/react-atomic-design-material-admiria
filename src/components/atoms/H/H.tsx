import React, {memo} from 'react';
import styled from 'styled-components';
import cn from 'classnames';

interface HProps {
    children?: React.ReactNode;
    className?: string;

    [prop: string]: any;

    onClick?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
}

const StyledH = styled.h1<HProps>`
  &.h1-c {font-size: 32px;}
  &.h2-c {font-size: 24px;}
  &.h3-c {font-size: 18.72px;}
  &.h4-c {font-size: 16px;}
  &.h5-c {font-size: 13.28px;}
  &.h6-c {font-size: 10.72px;}
  
  &.page-title {margin: 0; line-height: 70px; padding-left: 15px; font-size: 20px !important; font-weight: normal !important;}
`;

const H = ({
                  children,
                  className,
              }: HProps) => {
    const classCandidate = [className];
    const commonProps = {
    };
    return <StyledH {...commonProps} className={cn(classCandidate)}>
            {children}
        </StyledH>
};

export default memo(H);
