import React, {memo} from 'react';
import styled from 'styled-components';
import cn from 'classnames';

interface HRProps {
    className?: string;
}

const StyledHR = styled.hr<HRProps>`
  &.display {display: block !important;}
`;

const HR = ({
                  className,
              }: HRProps) => {
    const classCandidate = [className];
    const commonProps = {
    };
    return <StyledHR {...commonProps} className={cn(classCandidate)}/>

};

export default memo(HR);
