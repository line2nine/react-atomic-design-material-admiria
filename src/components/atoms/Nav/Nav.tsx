import React, {memo} from 'react';
import styled from 'styled-components';
import cn from 'classnames';

interface NavProps {
    children?: React.ReactNode;
    className?: string;

    [prop: string]: any;

    bgColor?: string;
    onClick?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
}

const StyledNav = styled.nav<NavProps>`
  &.navbar-custom {
    background-color: #ffffff;
    border: none;
    margin-bottom: 20px;
    -webkit-box-shadow: 0 2px 3px -2px rgba(0, 0, 0, 0.15);
    box-shadow: 0 2px 3px -2px rgba(0, 0, 0, 0.15);
  }
`;

const Nav = ({
                  children,
                  className,
                  bgColor
              }: NavProps) => {
    const classCandidate = [className];
    const commonProps = {
        bgColor
    };
    return <StyledNav {...commonProps} className={cn(classCandidate)}>
            {children}
        </StyledNav>
};

export default memo(Nav);
