import React, {memo} from 'react';
import styled from 'styled-components';
import cn from 'classnames';

interface HeaderProps {
    children?: React.ReactNode;
    className?: string;

    [prop: string]: any;

    bgColor?: string;
    onClick?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
}

const StyledHeader = styled.header<HeaderProps>`
  &.Header {
    position: fixed;
    width: 100%;
    height: 50px;
    background-color: #50a4dc;
  }
`;

const Header = ({
                 children,
                 className,
                 bgColor
             }: HeaderProps) => {
    const classCandidate = [className];
    const commonProps = {
        bgColor
    };
    return <StyledHeader {...commonProps} className={cn(classCandidate)}>
        {children}
    </StyledHeader>
};

export default memo(Header);
